\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{pdfpages}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{pdfpages}

%\beamerdefaultoverlayspecification{<+->} 
\usenavigationsymbolstemplate{}

\newcommand{\link}[2]{\href{#1}{\color{blue}{#2}}}
\usetheme{Boadilla}

\makeatother
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.4\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.6\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hspace*{7em}
    \hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatletter

\title{Support Vector Machines and Neural Networks}
\subtitle{5th European Data Science Summer School (ED3S)}
\author{Iaroslav Shcherbatyi}
\institute{\href{https://iaroslav-ai.github.io}{iaroslav-ai.github.io}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}
\setbeamertemplate{itemize items}[square]
\setbeamertemplate{enumerate items}[default]


%-------------------------------------

\begin{frame}{Agenda}

\begin{itemize}
	\item Linear Support Vector Machines
	\item Kernel SVM
	\item Some Tips and Tricks
	\item Shallow Artificial Neural Networks
	\item Training of Artificial Neural Networks
	\item Hyperparameter Tuning
	\item References
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Various output options}
\begin{itemize}
	\item Outputs are real values - regression task.\\ 
\textbf{Example}: Given features of a movie, what user rating is expected for this movie?
	\item Outputs are binary $\{-1, 1\}$ - binary classification. \\
\textbf{Example}: Given features of a company, forecast whether it will go bankrupt after 1 year.
\end{itemize}
\end{frame}

%-------------------------------------

\begin{frame}{Linear Support Vector Machines}
\begin{itemize}
	\item Binary classification model.
	\item Let matrix $X \in \mathbb{R}^{n \times m}$ represent dataset inputs, and $y \in \{-1,1\}^n$ outputs, where $n$ is number of observations, $m$ is number of features.
	\item Training is formulated as following optimization problem:
	$$ \underset{w \in \mathbb{R}^{m}, b \in \mathbb{R}}{\operatorname{argmin}} \ C [\sum_{i = 1...n} \max(1-(x_i^Tw+b)y_i, 0)] + \sum_{j = 1...m} w_i^2 $$
	\item Final model is of the form
	$$ f(x) = sign( w^Tx + b ) $$
	\item Presence of regularization allows to reduce effect of unnecessary features.
\end{itemize}
\end{frame}

%-------------------------------------

\begin{frame}{Linear Support Vector Machines, Hinge Loss}
\begin{itemize}
	\item Uses the Loss function (error function) of the form:
	$$ \max(1 - fy, 0) $$
	\item Easier to solve the optimization problem than with binary loss.
	\item More suitable for classification problems than squared loss.
	\item Example with $w \in \mathbb{R}, x=1.0, y=1.0$, and no bias term, $f = xw+b$.
	\item Online example here: kaggle.com/iaroslavai, see lecture supplements.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.3\textwidth]{figures/BinLoss.jpg}
	\includegraphics[width=0.3\textwidth]{figures/HingeLoss.jpg}
	\includegraphics[width=0.3\textwidth]{figures/SqLoss.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Linear Support Vector Machines, Regularization effects}
\begin{itemize}
	\item Decision boundary: points where the classifier function changes it's predictions. 
	\item For Linear SVM, these are all points $x \in \mathbb{R}^m$ such that $w^Tx+b=0$.
	\item Example: 2d classification dataset, where data was generated using $y = sign(x_2 + noise)$. Below is SVM decision boundary with L1 regularization:
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.4\textwidth]{figures/SVC_RC1.jpg}
	\includegraphics[width=0.4\textwidth]{figures/SVC_RC2.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Pros and Cons of Linear SVM}
Pros:
\begin{itemize}
	\item Linear classifier model with regularization often leads to better performance of the model compared to no regularization.
\end{itemize}
Cons:
\begin{itemize}
	\item Can only model linear relations between inputs and outputs.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.4\textwidth]{figures/SVC_NL1.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Non - linear (Kernel) SVM}
\begin{itemize}
	\item What if actual decision boundary is non - linear?
	\item There is still a way to use linear classifier: through the feature engineering!
	\item We are looking for a map of original features to some ``easier'' ones: 
	$$ f: \mathbb{R}^{m} \to \mathbb{R}^{m'} \quad m' \in \mathbb{N}$$
	\item Left: original features, right: $|x_1|, |x_2|$ features.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.4\textwidth]{figures/SVC_NL1.jpg}
	\includegraphics[width=0.4\textwidth]{figures/SVC_NL2.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Kernel SVM Model}
\begin{itemize}
	\item Reminder: linear classifier:
	$$ f(x) = sign(w^Tx+b) $$
	\item Let $X \in \mathbb{R}^{n, m}, y \in \mathbb{R}^m$ be a training dataset. Let $k: \mathbb{R}^m \times \mathbb{R}^m \to \mathbb{R}$ be a function which measures how similar or dissimilar two training points are. Then kernel SVM model is:
	$$ f(x) = sign( [\sum_{i = 1 ... n} w_i k(x, X_i) y_i] + b) $$	
	\item Equivalent to linear model with feature map  $K: \mathbb{R}^{m} \to \mathbb{R}^{n}$: 
	$$ f(x) = sign(w^TK(x)+b) $$
\end{itemize}
\end{frame}

%-------------------------------------

\begin{frame}{Kernel SVM Model: Gaussian Kernel}
\begin{itemize}
	\item A very popular choice for kernel function. Default in Scikit-Learn.
	\item Gaussian kernel function: $ k(x, x') = e^{-\gamma ||x-x'||_2^2} $
	\item Left: linear model, right: Kernel SVM with $\gamma=0.1$.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.45\textwidth]{figures/SVC_NL1.jpg}
	\includegraphics[width=0.45\textwidth]{figures/SVC_Kg01l.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Kernel SVM Model: Gaussian Kernel}
\begin{itemize}
	\item Gaussian kernel function: $ k(x, x') = e^{-\gamma ||x-x'||_2^2} $
	\item For a scalar input $x=1$, lets see effect of different $\gamma$:
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.32\textwidth]{figures/gk_01.jpg}
	\includegraphics[width=0.32\textwidth]{figures/gk_1.jpg}
	\includegraphics[width=0.32\textwidth]{figures/gk_10.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Kernel SVM Model: Gaussian Kernel}
\begin{itemize}
	\item Effects of different $\gamma$: noise in the data effects more the outputs of the model with larger $\gamma$.
	\item Optimal $\gamma$ should also be selected on the validation partition of dataset. 
	\item Example: a dataset, where point with distance to $(0, 0)$ point is classified as circle if it is less than 1.1. Noise added to outputs.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.32\textwidth]{figures/SVC_Kg01.jpg}
	\includegraphics[width=0.32\textwidth]{figures/SVC_Kg1.jpg}
	\includegraphics[width=0.32\textwidth]{figures/SVC_Kg10.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Kernel SVM Model: Training procedure}
\begin{itemize}
	\item Training proceudre of Kernel SVM can be formulated as follows$^1$:
	$$ \underset{w \in \mathbb{R}^{n}}{\operatorname{argmin}} \ [\sum_{i = 1...n} w_i] + [\sum_{i, j = 1...n} w_i w_j y_i y_j k(x_i, x_j)] $$
	subject to
	$$ 0 \leq w_i \leq C, \quad \sum_{i = 1...n} w_i y_i = 0 $$
	\item Not every kernel function works$^1$: needs to be a positive definite kernel function.
\end{itemize}
\footnotetext[1]{Schölkopf, Bernhard, and Alexander J. Smola. Learning with kernels: support vector machines, regularization, optimization, and beyond. MIT press, 2002, search for ``Duals, Using Kernels''}
\end{frame}

%-------------------------------------

\begin{frame}{Pros and Cons of Kernel SVM}
Pros:
\begin{itemize}
	\item Non - linear decision boundaries. In fact, arbitrary decision boundary can be approximated, given sufficient size of dataset.
	\item Can work on unstructured data: text, images, graphs, as long as an appropriate kernel function is defined.
\end{itemize}
Cons:
\begin{itemize}
	\item Training and evaluation of the model grows cubically with size of the dataset.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.7\textwidth]{figures/gaboost0.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{From binary classifier to multi-class classifier}
\begin{itemize}
	\item How to use $\{-1,1\}$ predictions to make estimations for $k>2$ classes?
	\item One - vs - all strategy: for every class predicted, train a model which estimates whether a particular input point is of that class. Leads to $k$ learning problems, and possibly imballanced dataset.
	\item One - vs - one strategy$^1$: train $0.5k(k-1)$ models, which distinguish between every two classes. Predictions are combined using majority vote:
$$ f(x)_{A, B} \to B $$	
$$ f(x)_{B, C} \to B $$	
$$ f(x)_{C, A} \to C $$	
$$ f(x)_{final} \to B $$	
	 Leads to more ballanced learning problems, but more computationally expensive.
\end{itemize}
\footnotetext[1]{Milgram, J., Cheriet, M. and Sabourin, R., 2006, October. “One against one” or “one against all”: Which one is better for handwriting recognition with SVMs?. }
% See more in https://hal.inria.fr/inria-00103955/document
\end{frame}

%-------------------------------------

\begin{frame}{Selection of learning algorithms}
%
\begin{itemize}
	\item It is often necessary to motivate a selection of models that you apply. One way is through ``average performance'' of model class on large set of datasets$^1$.
\end{itemize}
%
\begin{figure}[h]
	\includegraphics[width=0.8\textwidth]{figures/boosting_ranking.png}
\end{figure}
%
\footnotetext[1]{Olson, Randal S., et al. Data-driven advice for applying machine learning to bioinformatics problems. arXiv preprint arXiv:1708.05070 (2017).}
\end{frame}

%-------------------------------------

\begin{frame}{Cost sensitive learning}
\begin{itemize}
	\item Motivation$^1$: malignant (very bad) / benign (bad) cancer classification. False negatives are much worse than false positives.
	\item Assign higher costs to misclassification of malignant cases!
	$$ \underset{w \in \mathbb{R}^{m}, b \in \mathbb{R}}{\operatorname{argmin}} \ C [\sum_{i = 1...n} \mathbf{V(y_i)}\max(0, 1-(x_i^Tw+b)y_i)] + \sum_{j = 1...m} w_i^2 $$
	 \item Effects of different costs can be accessed using confusion matricies:
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.48\textwidth]{figures/costs_none.jpg}
	\includegraphics[width=0.48\textwidth]{figures/costs_scewed.jpg}
\end{figure}
\footnotetext[1]{W.N. Street, W.H. Wolberg and O.L. Mangasarian. Nuclear feature extraction for breast tumor diagnosis.}
\end{frame}

%-------------------------------------

\begin{frame}{Cross validation$^1$}
\begin{itemize}
	\item Problem: different partitions of training and evaluation data lead to different estimates of test performance. Example from previous slide, cost adjusted score, 20\% of data used for testing: 
	\begin{table}[]
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline
Run 1 & Run 2 & Run 3 & Run 4 & Run 5 \\ \hline
0.93  & 0.94  & 0.93  & 0.97  & 0.9   \\ \hline
\end{tabular}
\end{table}
	\item Solution: average over multiple partitions of dataset into training and testing. Such procedure is called a ``cross-validation''
	\item 3 fold cross - validation, 10 records dataset matrix example: \\
	Train on rows 1-6, test on 7-10; \\
	Train on rows 1-3, 7-10, test on 4-6; \\
	Train on rows 4-10, test on 1-3; \\
	Average obtained scores.
	\item Not necessary for problems of sufficiently large size.
\end{itemize}
\footnotetext[1]{Kohavi, Ron. A study of cross-validation and bootstrap for accuracy estimation and model selection.}
\end{frame}

%-------------------------------------

\begin{frame}{Tips and tricks}
\begin{itemize}
	\item Scale your features! Let $X \in \mathbb{R}^{n, m}, y \in \{0, 1\}^n$ represent your training dataset. Let $M, S \in \mathbb{R}^m$ be mean and standard deviation of columns of $X$. Search for the optimal model of the form:
	$$ y = f(t(x), w) \quad t(x)_i = (x_i - M_i) / S_i $$ 
	This very often leads to better performance.
	\item For large datasets, use subset of data for quick (and not very accurate) hyperparameter search. Right plot contains 4x less data.
\end{itemize}
\begin{figure}[h]
	\includegraphics[width=0.3\textwidth]{figures/SVC_NL2.jpg}
	\includegraphics[width=0.3\textwidth]{figures/SVC_NL2_small_data.jpg}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Artificial Neural Networks: Why Bother?}

\begin{itemize}
	\item Efficient at object detection in complex signals
	
See \link{https://modeldepot.github.io/tfjs-yolo-tiny-demo/}{Tiny YOLO web demo} \cite{redmon2016you}.	
	
	\item Popular in other application areas:
	\begin{itemize}
		\item Content Generation: \link{https://transcranial.github.io/keras-js}{digit generation}, or other visuals \cite{karras2017progressive}
		\item Automated Control: robotic \link{https://www.youtube.com/watch?v=knoOXBLFQ-s}{gymnastics} or \link{https://www.youtube.com/watch?v=fUyU3lKzoio}{navigation}. \cite{mnih2013playing}
	\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[width=0.247\textwidth]{figures/yolo_example.png}
\includegraphics[width=0.4\textwidth]{figures/robot_backflip.jpg}
\includegraphics[width=0.247\textwidth]{figures/progressive_gans.png}
\caption{From left to right: object detection, reinforcement learning, image generation.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Shallow ANN vs SVM}

\begin{itemize}
\only<1>{
\item Kernel SVM: Weighted sum of (dis)similarities with training points.
}
\only<2>{
\item Shallow ANN: Weighted sum of outputs of multiple neurons.
}
\item Let input to the model be $x \in \mathbb{R}^m$, and training dataset be $X \in \mathbb{R}^{n \times m}$. Bold arrows in figure below denote vector inputs, thin – scalar. Output of the model is denoted as $y \in \mathbb{R}$
\end{itemize}

\begin{figure}
\only<1>{
\includegraphics[width=\textwidth]{figures/ksvm_to_nn.pdf}
}
\only<2>{
\includegraphics[width=\textwidth]{figures/boost_to_nn.pdf}
}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Artificial Neural Networks: neuron types}

\begin{itemize}
\item Often as neuron blocks functions of the following form are considered:
$$ f(x) = \sigma(w^Tx+b), \quad \sigma: \mathbb{R} \to \mathbb{R} $$
\item Multiple types of non - linearities $\sigma$ used:
%
\begin{figure}
\captionsetup{justification=centering}
\begin{subfigure}{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/ReLU.jpg}
\caption{Rectified Linear Unit, $\sigma(a) = \max(0, a)$}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/Sigmoid.jpg}
\caption{Sigmoid function, $\sigma(a) = \frac{1}{1+e^{-a}}$}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/Tanh.jpg}
\caption{Hyperbolic tanhent, $\sigma(a) = \tanh(a)$}
\end{subfigure}
\end{figure}
%
\only<1>{
\item Question: why not use a linear activation function?
}
\only<2>{
\item Answer: in this case neural network is equivalent to linear model.
}
%
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Shallow ANN architecture summary}

\begin{itemize}
\item Like many other models, it's output is a weighted sum of many simple functions of input.
\item Like other types of ANN, inspired by human (biological) neural networks. \cite{london2005dendritic}
\item Hyperparameters to tune to make Shallow NN work:
\begin{itemize}
	\item Number of neurons in a layer
	\item Type of activation function
\end{itemize}
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Artificial Neural Networks: training procedure}

\begin{itemize}
\item For Kernel SVM, kernel is \textbf{not adjusted} during training. 
\item For boosting, a \textbf{single} weak learner is added and optimized at a time. 
\item For ANN, weights of \textbf{all} neurons are optimized during training. 
\item Here $w^{1...k} \in \mathbb{R}^{m+1}, w^{k+1} \in \mathbb{R}^{k}, $ and model input $x \in \mathbb{R}^m$.
\end{itemize}

\begin{figure}
\includegraphics[width=\textwidth]{figures/boost_to_nn_training.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Artificial Neural Networks: training procedure formulation}

\begin{itemize}
\item Let 
\begin{itemize}
	\item $\mathbb{W}$ denote set of all possible weights of ANN;
	\item $X \in \mathbb{R}^{n \times m}, y \in \mathbb{R}^{n}$ be inputs and outputs of a training dataset;
	\item $R: \mathbb{W} \to \mathbb{R}$ be a regularization function, e.g. $R(w) = ||w||_2^2$;
	\item $L: \mathbb{R} \times \mathbb{R} \to \mathbb{R}$ be a loss function, e.g. $L(a,b) = (a-b)^2$;
	\item $f: \mathbb{W} \times \mathbb{R}^m \to \mathbb{R}$ denote output of ANN for some $w$ and $x$;
\end{itemize}
\item Then training of ANN is formalized as something that you have already seen a few times:
$$ w^* = \underset { w \in \mathbb{W} }{ argmin } \ [\lambda R(w) + \sum_{i = 1...n} L(f(w, X_i), y_i)]$$ 
\item Here $w^*$ denotes the optimal weights.
\item Hyperparameter to tune: \textbf{Regularization strength} $ \mathbf{\lambda}$
\item Typically a flavour of Gradient Descent is used to optimize weights.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}

\begin{itemize}

\only<1>{\frametitle{Gradient Descent}}
\only<2>{\frametitle{Gradient Descent with Momentum}}
\only<3>{\frametitle{Stochastic Gradient Descent}}

\item Assume 
$\sum_{i = 1...n} L(f(w, X_i), y_i)$ is minimized. An algorithm is:
\begin{enumerate}
	\item Initialize weights $w$ of ANN at random, set $ \alpha > 0$\only<2->{, $M = null$}
\only<1>{
	\item \textbf{Update} $w := w - \alpha \sum_{i = 1...n} \nabla L(f(w, X_i), y_i)$ 
}
\only<2>{
	\item \textbf{Set} $M := \sum_{i = 1...n} \nabla L(f(w, X_i), y_i)$ if $M$ is $null$:
	\item \textbf{Update} $M := \mu M + \alpha \sum_{i = 1...n} \nabla L(f(w, X_i), y_i)$ \quad \cite{sutskever2013importance}
	\item \textbf{Update} $w := w - M$;
}
\only<3>{
	\item Select \textbf{random} subset $B$ of $k << n$ training points;
	\item Set $M := \sum_{x, y \in B} \nabla L(f(w, x), y)$ if $M$ is $null$:
	\item Update $M := \mu M + \alpha \sum_{x, y \in B} \nabla L(f(w, x), y)$ \quad \cite{sutskever2013importance}
	\item Update $w := w - M$;
}
	\item If not stopping criterion, then go to step 2
	\item Return $w$
\end{enumerate}

\item Stopping criterion: zero gradient, or maximum number of iterations.

\item See sgd-demo/sgd.html (https://goo.gl/sJVZ44) for example.
\end{itemize}

\begin{figure}
\only<1>{
\includegraphics[width=0.4\textwidth]{figures/gd.png}
}
\only<2>{
\includegraphics[width=0.3\textwidth]{figures/momentum.png}
\caption{Momentum helps to avoid getting stuck in local minimum.}
}
\only<3>{
\includegraphics[width=0.4\textwidth]{figures/batch.png}
\caption{A small subset of training points is sufficient to capture overall relationship between inputs and outputs.}
}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{SGD summary}

\begin{itemize}
	\item Initialize weights of ANN at random, and update them iteratively using gradient of training objective function.
	\begin{figure}
	\includegraphics[width=0.4\textwidth]{figures/gd.png}
	\end{figure}
	\item Other tricks: scaling of gradients, see for example Adam \cite{kingma2014adam}.
	\item In practice, you need to select these parameters for your NN to work:
	\begin{itemize}
		\item Update rate of weights of NN: \textbf{Learning rate}
		\item Size of data subset used at once to update weights: \textbf{Batch size}
		\item Stopping criterion
		\item Other optimizer specific parameters
	\end{itemize}
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Layerwise representation of NNs}

\begin{itemize}

\item Most architectures used in practice can be represented conveniently as a sequence of layers - collections of neurons.
\item Layer can be interpreted as a function, which takes vector as input and returns vector as output
\end{itemize}

\begin{figure}
\includegraphics[width=\textwidth]{figures/ffnn_layers.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Linear Layer}

\begin{itemize}
\item Applies linear transformation to it's input.
\end{itemize}

\begin{figure}
\includegraphics[width=\textwidth]{figures/matrix_layer.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Activation Layer}

\begin{itemize}
\item Applies activation function to every element of it's input.
\end{itemize}

\begin{figure}
\includegraphics[width=\textwidth]{figures/activation_layer.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Convenient representation of NN layers}

\begin{itemize}
\item A shallow neural network can be represented as a sequence of 3 layers.
\item Important from software point of view, as allows conceptually for speedups with parallel computing. \cite{abadi2016tensorflow}
\item Red blocks: no parameters, blue: with parameters. Here and afterwards, x refers to input of the block.
\end{itemize}

\begin{figure}
\includegraphics[width=\textwidth]{figures/ffnn_compact.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Classification with ANN}
%
\begin{itemize}
	\item Set the size of final layer to be N; Use layer that converts outputs to valid probabilities, such as softmax: $y_i = \frac{e^{y_i}}{\sum_j e^{y_j}}$.
	\item Convert output values in dataset to binary vectors;
	\begin{figure}
\includegraphics[height=0.3\textheight]{figures/onehot.pdf}
\end{figure}
	\item Use MSE or Categorical Cross - entropy loss.
\end{itemize}
%
$$ L(y', y) = -\sum_{i = 1...N} y_i log(y_i') $$
%
%
\begin{figure}
\begin{subfigure}{0.45\textwidth}
\textbf{Example task}: Classify whether there is a dog, a cat or none in a picture.
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
\centering
\includegraphics[width=0.44\textwidth]{figures/cat.jpg}
\end{subfigure}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Deep Feed - Forward NN}
%
\begin{itemize}
\item Assumes that the learning problem has a hierarchical structure.
\item Hyperparameter to tune:
\begin{itemize}
	\item Number of layers in the ANN.
\end{itemize}
\end{itemize}
%
\begin{figure}
\includegraphics[width=0.9\textwidth]{figures/deep_ffnn.pdf}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Aspects of NN}

\begin{itemize}
\item For the problem of constant complexity, larger neural nets are easier to train. One of the reasons why NNs are popular again (computational power increased $\to$ increased size of NNs) \cite{choromanska2015loss}
\item Often large NN with high regularization leads to good results \cite{karpathy2016cs231n}.
\end{itemize}

\begin{figure}
\includegraphics[width=0.7\textwidth]{figures/nn_loss.png}
\caption{Distributions of test losses w.r.t. ANN size \cite{choromanska2015loss}.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{ANN Software}

Popular ones:

\begin{itemize}
\item TensorFlow (Google): good for production deployment scenarios, but has a relatively high learning curve.
\item Keras (Google): high level library on top of TensorFlow.
\item PyTorch (Facebook), Chainer (Preferred Networks): good for research and quick prototyping. 
\item MXNet (Apache): has a strong support, has a somewhat higher learning curve than PyTorch / Chainer (in our experience). 
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Hyperparameter Optimization}

\begin{itemize}

\item Only in the lecture slides, 10+ parameters of the NN have been introduced, which need to be selected somehow.

\item Manual parameter tuning: takes a lot of your time. Solution: make machine do it, through the use of Bayesian Optimization! \cite{snoek2012practical}

\item \link{https://github.com/scikit-optimize/scikit-optimize}{Scikit-Optimize}: open source implementation of BO.

\end{itemize}

\begin{figure}
\includegraphics[width=0.5\textwidth]{figures/smbo.png}
\caption{A model of objective fit to the observations. Image taken from \link{https://github.com/scikit-optimize/scikit-optimize/blob/master/examples/bayesian-optimization.ipynb}{example notebook} of Scikit-Optimize.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{ANN: Advantages}

\begin{itemize}
\item Well suited for big data: Is not as computationally expensive as Kernel SVM or KNN for large amounts of data. 

\item State of the art: dedicated neural networks beat other methods on applications of computer vision and natural language processing.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{ANN: Disadvantages}

\begin{itemize}

\item Requires domain knowledge to get it right: Requires setting many parameters, like number and type of layers, type of neuron activation, training parameters, and many others, unlike few in KNN or SVM.

\item Black box model, in case of general purpose feed forward nets: Interpretation is complicated due to complex models and the topic being recent. Work is done in this area.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}[t,allowframebreaks]{References}
	\bibliographystyle{alpha}
	\bibliography{bibliography.bib}
\end{frame}


\end{document}

function clear_children(divId){
    var myNode = document.getElementById(divId);
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

/**
 * 
 * @param {*} divId name of the div
 * @param {*} elements list of children to ad
 * Creates a table with elements given in the list.
 * Nice for organization of different elements in a
 * row.
 */
function elements_to_div(divId, elements){
    var div = document.getElementById(divId)
    clear_children(divId)

    var table  = dce('table')
    div.appendChild(table)

    var row = dce('tr')
    table.appendChild(row)

    for(var child of elements){
        var td = dce('td')
        td.appendChild(child)
        row.appendChild(td)
    }
}

/**
 * Shorthand for document.createElement
 * @param {String} type Name of the element
 */
function dce(type){
    return document.createElement(type)
}

/**
 * Shorthand for conversion of text to node
 * @param {String} v Text to convert to node
 */
function txt(v){
    return document.createTextNode(v)
}

/**
 * Shorthand for document.getElementById
 * @param {String} id Id of element
 */
function gid(id){
    return document.getElementById(id)
}

function round(v, d){
    var v = Math.pow(10, d) * v
    v = Math.round(v)
    v = v * Math.pow(10, -d)
    return v
}

/**
 * Converts matrix given by a javascript array
 * into a table which can be visualized.
 * @param {Array} M Matrix or vector
 */
function matrix_to_table(M, headers=null, transpose=false, highlight_cell=null, editable=false){
    var result = dce('table')
    if(headers != null){
        var tr = dce('tr')
        var size = M[0].length > headers.length? headers.length : M[0].length
        for(var i=0; i<size; i++){
            var header = headers[i]
            var th = dce('th')
            tr.appendChild(th)
            th.appendChild(document.createTextNode(header))
        }
        result.appendChild(tr)
    }

    for(var rowi = 0; rowi < M.length; rowi++){
        var row = M[rowi]
        var tr = dce('tr')
        result.appendChild(tr)

        for(var i=0; i < row.length; i++){
            var cell = row[i]
            var td = dce('td')
            if(highlight_cell!=null){
                if(highlight_cell['ix'] == i){
                    td.style.border = "solid " + highlight_cell['color']
                    result.style.borderCollapse = "collapse";
                }
            }
            tr.appendChild(td)

            var cell_txt = ""+cell
            var cell_txt = Number.parseFloat(cell_txt).toFixed(5)
            if(cell_txt.indexOf('.') != -1){
                var pos = cell_txt.indexOf('.') + 5
                while(cell_txt[pos] == '0'){
                    pos -= 1
                    if(cell_txt[pos] == '.'){
                        pos -= 1
                        break
                    }
                }
                cell_txt = cell_txt.slice(0, pos+1)
            }
            var text_node = document.createTextNode(cell_txt);

            if(editable){
                td.contentEditable = "true"

                // clojure for automated editing

                var listener = function(text_node, M, rowi, i){
                    var result = function(){
                        var value = null;
                        try{
                            var nvalue = text_node.nodeValue
                            var nvalue = Number.parseFloat(nvalue)
                            value = nvalue
                        }catch{
                            value = M[rowi][i]
                        }
                        M[rowi][i] = value
                    }
                    return result
                }(text_node, M, rowi, i)

                td.addEventListener('input', listener)
            }

            td.appendChild(text_node)
        }
    }
    return result
}

function onehot(p, n){
    var result = []
    for(var i =0; i<n; i++){
        result.push(i==p?1.0:0.0)
    }
    return result
}

function convert_to_features(sentence){
    var result = []
    var words = {'good': 0, 'bad': 1, 'other': 2}

    // encode sentence here
    for(var word of sentence){
        if(!(word in words)){
            word = 'other'
        }
        result.push(onehot(words[word], 3))
    }
    return result
}

/**
 * Take a slice of array.
 * @param {Array of Array} X Array to take slice of
 */
function np_slice(X, dims){
    var result = []

    for(var row=0; row < X.length; row++){
        if(row < dims[0][0] || row >= dims[0][1]){
            continue
        }
        var tr = []
        for(var col=0; col < X[row].length; col++){
            if(col < dims[1][0] || col >= dims[1][1]){
                continue
            }
            tr.push(X[row][col])
        }
        result.push(tr)
    }

    return result
}

function np_transpose(M){
    var Mnew = []
    for(var i = 0; i < M[0].length; i++){
        var column = []
        for(var row of M){
            column.push(row[i])
        }
        Mnew.push(column)
    }
    M = Mnew
    return Mnew
}

function dot(a, b){
    var sum = 0;
    for(var i=0; i<a.length; i++){
        sum += a[i]*b[i]
    }
    return sum
}

function np_dot(A, B){
    var result = []
    var Bt = np_transpose(B)
    for(var row of A){
        var rr = [];
        for(var col of Bt){
            rr.push(dot(row, col))
        }
        result.push(rr)
    }
    return result
}

function np_add(A, B){
    var result = []

    for(var i=0; i<A.length; i++){
        var row = []
        for(var j=0; j<A[0].length; j++){
            row.push(A[i][j] + B[i][j])
        }
        result.push(row)
    }

    return result
}

function relu(A){
    var result = []

    for(var i=0; i<A.length; i++){
        var row = []
        for(var j=0; j<A[0].length; j++){
            row.push(A[i][j] > 0.0?A[i][j]:0.0)
        }
        result.push(row)
    }

    return result
}

class RNN{
    constructor(Wx=null, Wh=null){
        if(Wx == null){
            Wx = [[1, 0, 0], [0, 1, 0]]
        }
        if(Wh == null){
            Wh = [[1, 0], [0, 1]]
        }
        this.Wx = Wx
        this.Wh = Wh
        this.reset()
    }

    reset(){
        this.H = []
        this.h = []
        for(var i = 0; i < this.Wh[0].length; i++){
            this.h.push([0])
        }
    }

    f(x, return_ab = false){
        var a = np_dot(this.Wx, x)
        var b = np_dot(this.Wh, this.h)
        var r = np_add(a, b)
	r = relu(r)
        if(return_ab){
            return [r, a, b]
        }
        return r
    }

    step(x){
        this.h = this.f(x)
        this.H.push(this.h)
    }

    step_visualize(x){
        var hn, a, b
        [hn, a, b] = this.f(x, true)
        
        var result = [
            txt('max(0, ('),
            matrix_to_table(this.Wx, ['Wx'], false, null, true),
            matrix_to_table(x, ['x'], false),
            txt('='),
            matrix_to_table(a, [''], false),
            txt(') + ('),
            matrix_to_table(this.Wh, ['Wh'], false, null, true),
            matrix_to_table(this.h, ['h']),
            txt('='),
            matrix_to_table(b, [''], false),
            txt('))'),
            txt('='),
            matrix_to_table(hn, ["h'"])
        ]

        return result
    }
}

class RNN_vis{
    constructor(x_field, nn_field, y_field, sentence){
        this.x_field = x_field
        this.nn_field = nn_field
        this.y_field = y_field
        this.sentence = sentence
        this.reset()
    }

    reset(){
        this.x = convert_to_features(this.sentence)
        elements_to_div(this.nn_field, [])
        elements_to_div(this.y_field, [])
        this.rnn = new RNN()
        this.i = 0
        this.step()
    }

    restart(){
        this.i = 0;
        this.rnn.reset()
        this.step()
    }

    step(){
        if(this.i >= this.x.length){
            this.restart()
            return
        }
        elements_to_div(this.x_field, [
            txt('Inputs:  '),
            matrix_to_table(np_transpose(this.x), this.sentence, false, {'ix': this.i, 'color': 'grey'})
        ])

        var xn = np_transpose([this.x[this.i]])
        this.i += 1

        // visualize computation
        var vis = this.rnn.step_visualize(xn)
	vis.unshift(txt('RNN(x):'))
        elements_to_div(this.nn_field, vis)
        
        // do the actual step
        this.rnn.step(xn)

        // visualize the outputs of rnn
        elements_to_div(this.y_field, [
            txt('States:'),
            matrix_to_table(np_transpose(this.rnn.H), this.sentence, true)
        ])
    }

}

var vis = new RNN_vis('xinput', 'nncompute', 'youtput', ['start'])

function update_rnn_vis(){
    var sentence = gid('sentence').value
    var sentence = sentence.split(' ')

    vis.sentence = sentence
    vis.reset()
}

update_rnn_vis()

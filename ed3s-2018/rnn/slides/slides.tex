\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{pdfpages}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{pdfpages}

%\beamerdefaultoverlayspecification{<+->} %zeilenweises Erscheinen der Folien
\usenavigationsymbolstemplate{}


\newcommand<>{\textblue}[1]{{\color#2{blue}#1}}

\newcommand{\questionnairetwo}[3]{
\begin{block}{Frage}
\textbf{#1}
\vspace{-0.15cm}
\begin{columns}
\begin{column}{.45\textwidth}
\begin{itemize}
\item[\textblue{(A):}] #2
\end{itemize}
\end{column}
\begin{column}{.45\textwidth}
\begin{itemize}
\item[\textblue{(B):}] #3
\end{itemize}
\end{column}
\end{columns}
\end{block}
}

\newcommand{\questionnairefour}[5]{
\begin{block}{Frage}
\textbf{#1}
\vspace{-0.15cm}
\begin{columns}
\begin{column}{.45\textwidth}
\begin{itemize}
\item[\textblue{(A):}] #2
\item[\textblue{(B):}] #3
\end{itemize}
\end{column}
\begin{column}{.45\textwidth}
\begin{itemize}
\item[\textblue{(C):}] #4
\item[\textblue{(D):}] #5
\end{itemize}
\end{column}
\end{columns}
\end{block}
}

\newcommand{\link}[2]{\href{#1}{\color{blue}{#2}}}

\usetheme{Boadilla}

\makeatother
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.4\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.6\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle\hspace*{7em}
    \hfill\insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatletter

\title{Time Series with Recurrent Neural Networks}
\subtitle{5th European Data Science Summer School (ED3S)}
\author{Iaroslav Shcherbatyi}
\institute{\href{https://iaroslav-ai.github.io}{iaroslav-ai.github.io}}


\date{\today}
\begin{document}

\begin{frame}
\maketitle
\end{frame}
\setbeamertemplate{itemize items}[square]
\setbeamertemplate{enumerate items}[default]

%-------------------------------------

\begin{frame}{Time series: why bother?}

\begin{itemize}
	\item Often, data consist of sequences of mesurements.
	\item Humans analyse the sequence of past observations, and build models which allow us to forecast future observations.
	\item Doing such forecasting automatically is of great interest for a range of applications.
\end{itemize}
%
\begin{figure}
\includegraphics[height=0.3\textheight]{figures/obvious_future.png}
\caption{Almost any human has the ability to forecast from previous observations what will happen in future, with reasonable accuracy. Sometimes being more accurate, as for the situation depicted, is of great importance. Picture is from \link{https://youtu.be/J6XdouyIUtw?t=65}{this video}.}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Time series: video data}

\begin{itemize}
	\item A sequence of images $x \in R^{Time \times Width \times Height \times Color}$
	\item Example learning tasks:
	\begin{itemize}
		\item Autonomous driving
		\item Automated security surveillance
	\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[width=0.95\textwidth]{figures/video-seq.pdf}
\caption{Example sequence of frames consisting video time series.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Time series: audio data}

\begin{itemize}
	\item A sequence of vectors $x \in R^{Time \times Frequencies}$
	\item Example learning tasks:
	\begin{itemize}
		\item Speach recognition
		\item Heart anomaly detection
	\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[height=0.5\textheight]{figures/audio-seq.pdf}
\caption{Example spectrograms of audio segments containing patients heartbeat. It can be clearly seen that something is wrong with segment at the bottom.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Time series: market data}

\begin{itemize}
	\item A sequence of vectors $x \in R^{Time \times Features}$
	\item Example learning tasks:
	\begin{itemize}
		\item Forecast the price tomorrow
		\item Forecast the price growth or fall
		\item Learn buy / sell decisions to optimize returns
	\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[height=0.4\textheight]{figures/fintech-seq.pdf}
\caption{Futures data for ``Random Length Lumber'' commodity futures. Accurate forecasting of the opening price value or trend for next $n$ days based on today's historical data is of great interest for (automated) traders.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Time series: text}

\begin{itemize}
	\item Typical learning tasks:
	\begin{itemize}
		\item Error correction
		\item Sentiment analysis
		\item Language generation: \link{https://github.com/crazydonkey200/tensorflow-char-rnn}{Char RNN demo}
	\end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[height=0.4\textheight]{figures/char-rnn.pdf}
\caption{Char RNN generates text sequences character by character. Some initial text is used. For this text, the probabilities of next character being a, b, ... z are generated. The next character is sampled from this distribution, and concatenated to the text. The text is fed to the model again, to produce probabilities for the next character.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Time series: a typical learning problem}

\begin{itemize}
	\item Many learning problems with time series can be converted to the setup of mapping a sequence to some output.
	\item Sequences can be of arbitrary size!
\end{itemize}

\begin{figure}
\includegraphics[height=0.4\textheight]{figures/sequence-learning-problem.pdf}
\caption{A typical time series learning problem: come up with a model which for a given input sequence, represented as 2d matrix or real values, estimates accurately proper output.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Reduce TS to learning with vectors}

\begin{itemize}
	\item Convert sequences to vector representation: let $T$ be the size of the longest sequence in your dataset.
	\begin{itemize}
		\item Pad all sequences with zeros to the length $T$
		\item Convert sequence matricies to vectors by simply stacking columns.
	\end{itemize}
	\item With this sequence representation, you can use any of the previously discussed methods.
	\item Especially works for sequences of small length and feature number.
\end{itemize}
%
\begin{figure}
\includegraphics[height=0.4\textheight]{figures/sequence-to-vector.pdf}
\caption{Example transformation of sequence to vector.}
\end{figure}
%
\end{frame}


%-------------------------------------

\begin{frame}{Reduce TS to learning with vectors}
\begin{itemize}
	\item Scikit-Learn / Keras example: kaggle.com/iaroslavai/ed3s2018-day3-nn-svm-slides-supplement
	\item In the demo, it is shown that it is easy to transform sequences to vector of fixed size and use  machine learning techniques you have already seen.
	\only<2>{\item Uhm ... done!?}
\end{itemize}
\end{frame}

%-------------------------------------

\begin{frame}{Reduce TS to learning with vectors}

\begin{itemize}
	\item But: consider example of audio classification or speech recognition. If 1024 audio bands are used - 1024 features are considered in a single time step. 100+ time steps can be considered at a time.

\end{itemize}

\centering

100 x 1024 = 102400 features

\begin{itemize}
	\item But what if 100 steps is not enough?
	\item Number of weights of the model grows with number of features. With larger number of model weights it is typically easier to overfit!
	\item Would be good to have models whose size \textbf{does not} depend on the length of the input sequence.
\end{itemize}
%
\begin{figure}
\includegraphics[width=0.95\textwidth]{figures/audio-seq-h.pdf}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Why too large feature vector is bad}

\begin{itemize}
	\item It is more computationally expensive.
	\item With great size of feature vector comes great responsibility to not overfit. 
\end{itemize}

\begin{table}[]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Pokemons collected} & \textbf{GPA BA} & \textbf{Study hours / day} & \textbf{GPA MS} \\ \hline
0                         & 1.7             & 12.1                       & 1               \\ \hline
0                         & 1.7             & 6.1                        & 2               \\ \hline
0                         & 3.3             & 14.3                       & 1.7             \\ \hline
0                         & 3               & 1                          & 3.7             \\ \hline
0                         & 2.3             & 5                          & 2.3             \\ \hline
\textbf{5}                         & 1.7             & 9.1                        & 1.3             \\ \hline
0                         & 1.3             & 3                          & 2               \\ \hline
\end{tabular}
\caption{``Pokemons collected'' feature is useful due to noise in the data, and leads to overfitting.}
\end{table}

\end{frame}

%-------------------------------------

\begin{frame}{Manual solution: handcrafted features!}

\begin{itemize}
	\item One way to reduce number of features.
	\item Example: for sentiment analysis, search for words like ``good'', ``bad'', ``happy'', ``angry''
	\item Convert text to a binary feature vector, where every feature correspond to particular word present 1 or absent 0.
	\item Counterexample for such approach:
\end{itemize}
\centering
``I am so angry that I have not seen this movie earlier. Any movie looks bad compared to this one.''
%
\begin{figure}
\includegraphics[height=0.35\textheight]{figures/sequence-to-manual-features.pdf}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Vanilla Recurrent Neural Network}

\begin{itemize}
	\item A neural network which can extract features from sequences.
	\item Let $x \in \mathbf{R}^{l, m}$ be the input, and $p$ be the desired number of features.
	\item Let the weights of the network be $W \in \mathbf{R}^{m, p}, W' \in \mathbf{R}^{p, p}, b \in \mathbf{R}^{p} $. 
	\item The output of such network is calculated as follows:
	\begin{itemize}
		\item Set $h$ to be vector of zeroes, $i=1$, $h \in \mathbf{R}^p$.
		\item Update $h = \sigma(W^Tx_i+W'^Th+b)$
		\item Increase $i$ by one. If $i \leq l$ - return to step 2. 
		\item Return $ffnn(h)$
	\end{itemize}
	\item Note: the size of network does not depend on length of input sequence.
\end{itemize}
%
\begin{figure}
\includegraphics[height=0.35\textheight]{figures/sequence-to-rnn-features.pdf}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Vanilla RNN demo}

\begin{itemize}
	\item see rnn-demo/rnn.html (https://goo.gl/sJVZ44) for an interactive demonstration. See instructions in the file for more details.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{How it is typically presented}

\begin{itemize}
	\item As a network with recurrent connection, for the hidden state, or ``memory'' vector. 
	\item Alternatively, as a feed forward neural network with special architecture.
	\item Recurrent neural networks map 2d sequence input to 2d sequence of memory vectors.
\end{itemize}
%
\begin{figure}
\includegraphics[width=\textwidth]{figures/RNN-unrolled.png}
\caption{RNN: a neural network with ``recurrent'' connection, which stores information about previously seen elements in the sequence. Source: http://colah.github.io/posts/2015-08-Understanding-LSTMs/.}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Recurrent Neural Networks: training procedure formulation}

\begin{itemize}
\item Same as for other NN types!
\item Let 
\begin{itemize}
	\item $\mathbb{W}$ denote set of all possible weights of ANN;
	\item $X \in \mathbb{R}^{n \times m}, y \in \mathbb{R}^{n}$ be inputs and outputs of a training dataset;
	\item $R: \mathbb{W} \to \mathbb{R}$ be a regularization function, e.g. $R(w) = ||w||_2^2$;
	\item $L: \mathbb{R} \times \mathbb{R} \to \mathbb{R}$ be a loss function, e.g. $L(a,b) = (a-b)^2$;
	\item $f: \mathbb{W} \times \mathbb{R}^m \to \mathbb{R}$ denote output of RNN for some $w$ and $x$;
\end{itemize}
\item Then training of RNN is formalized as something that you have already seen a few times:
$$ w^* = \underset { w \in \mathbb{W} }{ argmin } \ [\lambda R(w) + \sum_{i = 1...n} L(f(w, X_i), y_i)]$$ 
\item Here $w^*$ denotes the optimal weights.
\item Typically a flavour of Gradient Descent is used to optimize weights.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Keras example of RNN layer}

\begin{itemize}
	\item See rnnlayer.ipynb for example code of RNN layer in Keras.
\end{itemize}
%
\end{frame}

%-------------------------------------

\begin{frame}{Learning formulations}

\begin{itemize}
	\item One to many, many to one, many to many
	\item All can be generalized by the many to one mapping.
\end{itemize}
%
\begin{figure}
\includegraphics[height=0.5\textheight]{figures/learning-formulations.png}
\caption{Example sequence problems that can occur in practice. Source: \cite{fei2015stanford}. All of those can be cast as simple many to one prediction task.}
\end{figure}
%
\end{frame}

%-------------------------------------

\begin{frame}{Gradient Descent Reminder}

\begin{itemize}
	\item Assume 
	$F(w)$ is minimized. An algorithm is: 
	\begin{enumerate}
		\item Initialize weights $w$ of ANN at random, set $ \alpha > 0$
		\item \textbf{Update} $w := w - \alpha \nabla_w F(w)$ 
		\item If not stopping criterion, then go to step 2
		\item Return $w$
	\end{enumerate}
	
	\item Stopping criterion: zero gradient, or maximum number of iterations.

	\item See sgd-demo/sgd.html (https://goo.gl/sJVZ44) for example.
\end{itemize}

\begin{figure}
\includegraphics[width=0.4\textwidth]{figures/gd.png}
\caption{Gradient Descent is an iterative algorithm, which utilizes the gradient of error function to minimize error w.r.t. weights ot the neural network.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{Problems with Simple RNN}

\begin{itemize}
	\item Wanishing and expoding gradients!
	\item Example: rnn of scalar feature, with hidden layer of size 1, no bias term, and linear activation $\sigma(a) = a$.
	\item Formula of RNN hidden layer after $L$ input:
	$$ RNN(x, w) = (((w x_1)w'+w x_2)w'...+w x_L) = \sum_{i = 1 ... L} w'^{L-i}w x_i $$
	\item Gradient of $w'$:
	$$ \frac{dRNN}{dw'} = \sum_{i = 1 ... L} (L-i)w'^{L-i-1}wx_i $$
	\item If $w' > 1$: the gradient can become very large: unstable training.
	\item If $0 < w' < 1$: the terms with small $i$ do not affect the outputs.
	\item Similar effect for full blown RNN's!
\end{itemize}
%
\end{frame}

%-------------------------------------

\begin{frame}{Gradient explosion / vanishing demo}

\begin{itemize}
	\item see rnn-demo/rnn.html (https://goo.gl/sJVZ44) for an interactive demonstration. See instructions in the file for more details.
\end{itemize}

\end{frame}

%-------------------------------------

\begin{frame}{Gradient issues: motivation for LSTM}
%
\begin{itemize}
	\item Both vanishing and exploding gradient issues are magnified with larger sequence sizes.
	\item To remedy these adverse effects, LSTM \cite{schmidhuber2015deep} was developed.
\end{itemize}
%
\begin{figure}
\includegraphics[height=0.4\textheight]{figures/LSTM3-chain.png}
\caption{LSTM architecture. The change of long term memory cell is made via addition, and multiplication ith sigmoid, whose values are in $[0, 1]$. Source: http://colah.github.io/posts/2015-08-Understanding-LSTMs/.}
\end{figure}
\end{frame}

%-------------------------------------

\begin{frame}{Other RNN variants}
%
\begin{itemize}
	\item Gated Recurrent Unit: a bit more simple than LSTM, but retains it's good training properties.
	\item Many other variants \cite{schmidhuber2015deep}
	\item An active research area.
\end{itemize}
%
\end{frame}

%-------------------------------------

\begin{frame}{General advice on RNNs}
%

\begin{itemize}
\item For the problem of constant complexity, larger neural nets are easier to train. One of the reasons why NNs are popular again (computational power increased $\to$ increased size of NNs) \cite{choromanska2015loss}
\item Use large neural networks with high regularization \cite{zaremba2014recurrent, karpathy2016cs231n} - these are likely to lead to good results.
\end{itemize}

\begin{figure}
\includegraphics[width=0.7\textheight]{figures/nn_loss.pdf}
\caption{Larger neural networks are easier to train. With larger neural networks, the result of training algorithm depends less on the initialization, and typically leads to better performance \cite{zaremba2014recurrent}.}
\end{figure}

\end{frame}

%-------------------------------------

\begin{frame}{RNNs: pros}
%
\begin{itemize}
	\item Works well with long sequences - scales computationaly and parameter-wise well.
	\item Alleviates the need for manual feature engineering.
	\item Sets a state of the art in sequence processing fields, such as text processing.
\end{itemize}
%
\end{frame}

%-------------------------------------

\begin{frame}{RNNs: cons}
%
\begin{itemize}
	\item As with deep learning in general, it is not trivial to understand why the model makes particular decisions.
	\item Requires parameter tuning to get the best performance. 
\end{itemize}
%
\end{frame}

%-------------------------------------


\begin{frame}[t,allowframebreaks]{References}
	\bibliographystyle{alpha}
	\bibliography{bibliography.bib}
\end{frame}

\end{document}
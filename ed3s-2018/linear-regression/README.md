# Hands - on session for Linear Regression and KNN presentation

To access the hands - on material, please do the following:
* Register on [kaggle.com](https://kaggle.com)
* Fork [this notebook](https://www.kaggle.com/iaroslavai/ed3s2018-day1-linearregression-knn).
* Press "Edit Notebook" to start the interactive session.
